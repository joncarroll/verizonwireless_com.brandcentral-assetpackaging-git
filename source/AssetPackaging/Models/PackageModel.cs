using System;
using System.Collections.Generic;

namespace AssetPackaging.Models {
    public class PackageModel {
        public string DownloadName { get; set; }
        public string SourceDirectory { get; set; }
        public string DestinationDirectory { get; set; }
        public string Hash { get; set; }
        public List<PackageFileModel> Files { get; set; }
    }

    public class PackageFileModel { 
        public string Name { get; set; }
        public string Path { get; set; }
        public string Id { get; set; }
        public string S3Key { get; set; }
    }
}