using System;
using System.Collections.Generic;

namespace AssetPackaging.Models {
    public class ArgumentsModel {
        public bool Verbose { get; set; }
        public string AssetId { get; set; }
    }

    public enum RunMode {
        RunAll,
        RefreshJobs,
        RunPendingJobs,
        DeleteExistingPackages,
        GetTotalContentLength,
        DeleteAllJobs
    }
}