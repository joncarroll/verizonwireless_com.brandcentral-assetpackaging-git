﻿using System;
using System.IO;
using Amazon.Extensions;
using AssetPackaging.Config;
using AssetPackaging.Domain;
using AssetPackaging.Services;
using AssetPackaging.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;

namespace AssetPackaging {
    class Program {
        static void Main (string[] args) {
            var serviceProvider = ConfigureServices ()
                .BuildServiceProvider ();

            serviceProvider.GetService<App> ().Run (args).GetAwaiter ().GetResult ();
        }

        private static IServiceCollection ConfigureServices () {
            IServiceCollection services = new ServiceCollection ();

            var builder = new ConfigurationBuilder ()
                .SetBasePath (Directory.GetCurrentDirectory ())
                .AddJsonFile ("appsettings.json", optional : true, reloadOnChange : true)
                .AddJsonFile ("appsettings.local.json", optional : true, reloadOnChange : true);

            var configuration = builder.Build ();

            //set up logging
            var loggerFactory = LoggerFactory.Create (builder => {
                builder
                    .ClearProviders ()
                    .AddConfiguration (configuration.GetSection("Logging"))
                    .AddConsole (options => {
                        options.IncludeScopes = true;
                    })
                    .AddNLog ();
            });
            services.AddLogging ();
            services.AddSingleton (loggerFactory);

            services.AddOptions ();
            services.Configure<AWSConfig> (configuration.GetSection ("AWSConfiguration"));
            services.Configure<AppConfig> (configuration.GetSection ("AppConfig"));

            //set up database context
            services.AddDbContext<ResourceLibraryContext> (options =>
                options.UseSqlServer (configuration.GetConnectionString ("ResourceLibrary")));

            //add services
            services.AddScoped<IS3Service, S3Service> ();
            services.AddTransient<IRlDbService, RLDbService> ();
            services.AddTransient<IPackagingService, PackagingService> ();
            services.AddTransient<App> ();

            return services;
        }
    }
}