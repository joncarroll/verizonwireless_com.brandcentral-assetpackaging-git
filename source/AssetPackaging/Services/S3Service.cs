using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using AssetPackaging.Config;
using AssetPackaging.Services.Interfaces;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace AssetPackaging.Services {
    public class S3Service : IS3Service {
        private readonly AWSConfig _config;
        private readonly AmazonS3Client _client;
        private readonly ILogger<S3Service> _logger;
        public S3Service (IOptions<AWSConfig> config, ILogger<S3Service> logger) {
            _logger = logger;
            _config = config.Value;
            _logger.LogDebug ("Initializing S3 Service.");
            _logger.LogDebug ("Bucket: " + _config.Bucket);
            _logger.LogDebug ("Access Key: " + _config.AccessKey);
            _logger.LogDebug ("Region: " + _config.Region);
            _client = new AmazonS3Client (
                new BasicAWSCredentials (_config.AccessKey, _config.SecretKey), RegionEndpoint.USEast1
            );
        }

        public async Task<GetObjectMetadataResponse> GetObjectMetadataAsync (string key) {
            var response = await GetObjectMetadataAsync (new List<string> () { key });
            return response.FirstOrDefault ();
        }

        public async Task<List<GetObjectMetadataResponse>> GetObjectMetadataAsync (List<string> keys) {
            try {
                var retVal = new List<GetObjectMetadataResponse> ();
                _logger.LogDebug ($"GetObjectMetadataAsync Start");
                _logger.LogDebug ($"Getting metadata for {keys.Count} objects.");
                var errors = new List<string> ();
                foreach (var key in keys) {
                    var request = new GetObjectMetadataRequest {
                        BucketName = _config.Bucket,
                        Key = key
                    };
                    try {
                        var response = await _client.GetObjectMetadataAsync (request);
                        retVal.Add (response);
                    } catch (AmazonS3Exception e) {
                        if (e.ErrorCode == "NotFound") {
                            //_logger.LogDebug ($"Failed to find {key}.");
                            errors.Add (key);
                        } else throw;
                    }

                }
                _logger.LogDebug ($"GetObjectMetadataAsync End");
                if (errors.Count > 0) {
                    _logger.LogError ($"Failed to find {errors.Count} objects from S3.");
                }
                return retVal;
            } catch (AmazonS3Exception e) {
                _logger.LogError ("Error encountered ***. Message:'{0}' when writing an object", e.Message);
                throw e;
            } catch (Exception e) {
                _logger.LogError ("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
                throw e;
            }
        }

        public async Task<MemoryStream> GetObjectMemoryStreamAsync (string key) {
            try {
                _logger.LogDebug ($"GetObjectMemoryStreamAsync Start");
                var request = new GetObjectRequest {
                    BucketName = _config.Bucket,
                    Key = key
                };

                using (GetObjectResponse response = await _client.GetObjectAsync (request))
                using (Stream responseStream = response.ResponseStream) {
                    MemoryStream memResponseStream = new MemoryStream ();
                    responseStream.CopyTo (memResponseStream);
                    memResponseStream.Position = 0;
                    _logger.LogDebug ($"GetObjectMemoryStreamAsync End");
                    return memResponseStream;
                }
            } catch (AmazonS3Exception e) {
                _logger.LogError ("Error encountered ***. Message:'{0}' when writing an object", e.Message);
                throw e;
            } catch (Exception e) {
                _logger.LogError ("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
                throw e;
            }

        }

        public async Task DownloadObjectAsync (string key, string destinationPath) {
            try {
                _logger.LogDebug ($"DownloadObjectAsync Start");
                var request = new GetObjectRequest {
                    BucketName = _config.Bucket,
                    Key = key
                };

                using (var response = await _client.GetObjectAsync (request)) {
                    await response.WriteResponseStreamToFileAsync (destinationPath, false, default (CancellationToken));
                }
                _logger.LogDebug ($"DownloadObjectAsync End");
            } catch (AmazonS3Exception e) {
                _logger.LogError ("Error encountered ***. Message:'{0}' when writing an object", e.Message);
                throw e;
            } catch (Exception e) {
                _logger.LogError ("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
                throw e;
            }
        }

        public async Task UploadObjectAsync (string filePath, string destinationKey, string downloadName) {
            try {
                _logger.LogDebug ($"UploadObjectAsync Start");
                var fileTransferUtility =
                    new TransferUtility (_client);

                var fileTransferUtilityRequest = new TransferUtilityUploadRequest {
                    BucketName = _config.Bucket,
                    FilePath = filePath,
                    StorageClass = S3StorageClass.StandardInfrequentAccess,
                    PartSize = 6291456, // 6 MB.
                    Key = destinationKey,
                    CannedACL = S3CannedACL.PublicRead
                };
                fileTransferUtilityRequest.Headers.ContentDisposition = $"attachment; filename={downloadName}";

                await fileTransferUtility.UploadAsync (fileTransferUtilityRequest);
                _logger.LogDebug ($"UploadObjectAsync End");
            } catch (AmazonS3Exception e) {
                _logger.LogError ("Error encountered on server. Message:'{0}' when writing an object", e.Message);
                throw e;
            } catch (Exception e) {
                _logger.LogError ("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
                throw e;
            }
        }

        public async Task<List<S3Object>> ListObjectsAsync (string prefix) {
            try {
                _logger.LogDebug ($"ListObjectsAsync Start");
                var request = new ListObjectsV2Request () {
                    BucketName = _config.Bucket,
                    Prefix = prefix,
                    MaxKeys = 50
                };

                ListObjectsV2Response response;
                var retVal = new List<S3Object> ();

                do {
                    response = await _client.ListObjectsV2Async (request);
                    if (response != null && response.S3Objects != null) {
                        retVal.AddRange (response.S3Objects);
                    }
                    request.ContinuationToken = response.NextContinuationToken;
                } while (response.IsTruncated);
                _logger.LogDebug ($"ListObjectsAsync End");
                return retVal;
            } catch (AmazonS3Exception e) {
                _logger.LogError ("S3 error occurred. Exception: " + e.ToString ());
                throw e;
            } catch (Exception e) {
                _logger.LogError ("Exception: " + e.ToString ());
                throw e;
            }
        }

        public async Task DeleteObjectsAsync (List<string> objectKeys) {
            try {
                _logger.LogDebug ("DeleteObjectsAsync Start");
                var request = new DeleteObjectsRequest () {
                    BucketName = _config.Bucket
                };
                _logger.LogDebug ("Keys to be removed:");
                foreach (var key in objectKeys) {
                    _logger.LogDebug (key);
                    request.AddKey (key, null);
                }

                await _client.DeleteObjectsAsync (request);
                _logger.LogDebug ("DeleteObjectsAsync End");
            } catch (AmazonS3Exception e) {
                _logger.LogError ("S3 error occurred. Exception: " + e.ToString ());
                throw e;
            } catch (Exception e) {
                _logger.LogError ("Exception: " + e.ToString ());
                throw e;
            }
        }
    }
}