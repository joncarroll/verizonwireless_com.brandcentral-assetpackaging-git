using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AssetPackaging.Config;
using AssetPackaging.Domain;
using AssetPackaging.Domain.Models;
using AssetPackaging.Helpers;
using AssetPackaging.Models;
using AssetPackaging.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace AssetPackaging.Services {
    public class PackagingService : IPackagingService {

        private readonly IRlDbService _dbService;
        private readonly ILogger<PackagingService> _logger;
        private readonly IS3Service _s3;
        private readonly AppConfig _config;
        private readonly string _workingDirectory;
        private readonly string _filesDirectory;
        private readonly string _packageDirectory;

        public PackagingService (IRlDbService db, ILogger<PackagingService> logger, IS3Service s3, IOptions<AppConfig> config) {
            _logger = logger;
            _dbService = db;
            _s3 = s3;
            _config = config.Value;
            _workingDirectory = Path.GetDirectoryName (Assembly.GetEntryAssembly ().Location);
            _filesDirectory = Path.Combine (_workingDirectory, _config.LocalFilesPath);
            _packageDirectory = Path.Combine (_workingDirectory, _config.LocalPackagePath);
        }

        private void directoryInit (string jobId) {
            _logger.LogDebug ($"Initializing Job: {jobId}");
            if (!Directory.Exists (Path.Combine (_filesDirectory, jobId))) {
                Directory.CreateDirectory (Path.Combine (_filesDirectory, jobId));
                Directory.CreateDirectory (Path.Combine (_filesDirectory, jobId, "all"));
            }
            if (!Directory.Exists (Path.Combine (_packageDirectory, jobId))) {
                Directory.CreateDirectory (Path.Combine (_packageDirectory, jobId));
            }
        }
        private void disposeJob (string jobId) {
            if (!_config.DisposeLocalFiles) return;

            _logger.LogDebug ($"Disposing Job: {jobId}");
            if (Directory.Exists (Path.Combine (_filesDirectory, jobId))) {
                Directory.Delete (Path.Combine (_filesDirectory, jobId), true);
            }
            if (Directory.Exists (Path.Combine (_packageDirectory, jobId))) {
                Directory.Delete (Path.Combine (_packageDirectory, jobId), true);
            }
        }

        public long GetSizeOfAllActiveFiles () {
            var activeAndCurrentFileVersions = _dbService.GetActiveAssets ()
                .SelectMany (x => x.AssetFiles.Select (x => x.File))
                .Where (x => x.CurrentVersionId != null && (x.FileVersions != null && x.FileVersions.Count > 0))
                .Select (x => x.FileVersions.FirstOrDefault (y => y.Id == x.CurrentVersionId))
                .ToList ();

            _logger.LogInformation ($"{activeAndCurrentFileVersions.Count} current and active file versions found");

            var s3Keys = new List<string> ();

            activeAndCurrentFileVersions.ForEach (x => {
                s3Keys.Add (x.GenerateAwsKey (_config.AWSFilesPath));
            });

            var fileMetas = _s3.GetObjectMetadataAsync (s3Keys).GetAwaiter ().GetResult ();

            var totalContentLength = fileMetas.Sum (x => x.ContentLength);
            _logger.LogInformation ($"Total Content-Length: {totalContentLength}");

            return totalContentLength;
        }

        public async Task<int> RefreshJobs () {

            _logger.LogInformation ("===== Refreshing jobs =====");

            //get all active assets that don't have a package hash set.
            var staleAssets = _dbService.GetActiveStaleAssets ();
            _logger.LogInformation ($"{staleAssets.Count} active assets found that have more than one file and no package hash set.");
            _logger.LogInformation("Creating jobs...");
            int counter = 0;
            foreach (var staleAsset in staleAssets) {
                var files = _dbService.GetCurrentFileVersions (staleAsset);
                _logger.LogDebug ($"Creating job for Asset {staleAsset.Id}");
                _logger.LogDebug ($"{files.Count()} current file versions found.");

                var hash = AssetPackagingHelper.ComputePackageHash (files.Select (x => x.Id).ToList ());
                _logger.LogDebug ($"Package hash for Asset {staleAsset.Id}: {hash}");

                var existingJob = _dbService.GetPackageJob (staleAsset.Uid, hash);
                if (existingJob != null && existingJob.HasError && existingJob.RetryCount >= _config.MaxRetryCount) {
                    _logger.LogDebug ($"Job #{existingJob.Id} already exists for Asset {staleAsset.Id} with a package hash of {hash}. It has errored out and reached max retry count.");
                    continue;
                }

                var newJob = _dbService.CreatePackageJob (staleAsset.Uid, hash);
                _logger.LogDebug ($"Job #{newJob.Id} created.");

                //delete existing packages if they exist
                await DeleteExistingPackages (staleAsset.Id.ToLower ());
                counter++;
            }
            _logger.LogInformation ($"===== {counter} new jobs created. =====");
            return counter;
        }

        public async Task RunPendingJobs () {
            _logger.LogInformation ("===== Running pending jobs =====");
            var pendingJobs = _dbService.GetPendingJobs ();

            _logger.LogInformation ($"{pendingJobs.Count} pending jobs found");

            int counter = 1;
            bool retry = false;
            foreach (var job in pendingJobs) {
                _logger.LogInformation ($"------------------------------------------ Starting job # {job.Id}  [ {counter} / {pendingJobs.Count} ] ");

                try {
                    await RunJob (job);
                    _logger.LogInformation ($"------------------------------------------ Job #{job.Id} - SUCCESS");
                } catch (Exception ex) {
                    _dbService.MarkPackageJobAsErrored (job, ex);
                    retry = true;
                    _logger.LogWarning ($"------------------------------------------ Job #{job.Id} - ERROR (RetryCount: {job.RetryCount}");
                }
                counter++;
            }

            _logger.LogInformation ("===== All pending jobs finished =====");


            if (retry) {
                await RetryJobs();
            }
        }

        public async Task RetryJobs () {

            var erroredJobs = _dbService.GetErroredPackageJobs (_config.MaxRetryCount);
            if (erroredJobs != null && erroredJobs.Count > 0) {
                _logger.LogInformation ($"===== Retrying {erroredJobs.Count} errored jobs =====");
            } else {
                _logger.LogInformation ("fuck yea, no errors");
                return;
            };

            int counter = 1;
            foreach (var job in erroredJobs) {
                var retryJob = _dbService.IncrementRetryCount (job);
                while (retryJob.RetryCount < 3) {
                    _logger.LogInformation ($"[ {counter} / {erroredJobs.Count} ] Retrying job # {retryJob.Id}");
                    try {

                        await RunJob (retryJob);
                        _logger.LogInformation ($"[ {counter} / {erroredJobs.Count} ] Job #{retryJob.Id} - SUCCESS");
                    } catch (Exception ex) {
                        _dbService.MarkPackageJobAsErrored (retryJob, ex);
                        retryJob = _dbService.IncrementRetryCount (retryJob);
                        _logger.LogWarning ($"[ {counter} / {erroredJobs.Count} ] Job #{retryJob.Id} - ERROR (RetryCount: {retryJob.RetryCount})");
                    }
                }
                counter++;
            }
            _logger.LogInformation ("===== Finished retrying errored jobs =====");
        }

        public async Task RunJob (PackageJob job) {

            var jobId = job.Id.ToString ();
            _dbService.MarkPackageJobAsStarted (job);

            directoryInit (jobId);

            var tempFilePath = Path.Combine (_filesDirectory, jobId);
            var tempFilePathAll = Path.Combine (_filesDirectory, jobId, "all");
            var tempPackagePath = Path.Combine (_packageDirectory, jobId);

            try {
                var asset = _dbService.GetAsset (job.AssetId);
                var fileVersions = _dbService.GetCurrentFileVersions (asset);
                if (fileVersions.Count <= 1) {
                    _logger.LogWarning ($"THIS SHOULD NOT HAPPEN ----- Deleting job {job.Id.ToString()} because it only has one file.");
                    _dbService.DeletePackageJob (job);
                    disposeJob (jobId);
                    return;
                }

                var packages = new List<PackageModel> ();

                //create packagemodel for ALL files
                var allPackage = new PackageModel ();
                allPackage.Files = new List<PackageFileModel> ();
                _logger.LogInformation ($"Downloading {fileVersions.Count} files from S3...");

                int counter = 1;
                foreach (var fv in fileVersions) {
                    var packageFile = new PackageFileModel ();
                    packageFile.S3Key = fv.GenerateAwsKey (_config.AWSFilesPath);
                    packageFile.Path = Path.Combine (tempFilePathAll, fv.Name);
                    packageFile.Name = Path.GetFileNameWithoutExtension (fv.Name);
                    packageFile.Id = fv.Id;

                    //go ahead and download file from S3 to disk
                    _logger.LogInformation ($"File {counter} / {fileVersions.Count} - Downloading...");
                    await _s3.DownloadObjectAsync (packageFile.S3Key, packageFile.Path);
                    allPackage.Files.Add (packageFile);
                    counter++;
                }
                //generate hash based off all fileversion ids
                allPackage.Hash = AssetPackagingHelper.ComputePackageHash (
                    allPackage.Files.Select (x => x.Id).ToList ()
                );
                if (allPackage.Hash != job.Hash) {
                    throw new Exception ($"Job #{job.Id} - Hash on job does not match hash computed for Asset. This could be an unfortunate timing issue or it could be that my code sucks. This should not happen.");
                }
                allPackage.SourceDirectory = tempFilePathAll;
                allPackage.DestinationDirectory = Path.Combine (tempPackagePath, allPackage.Hash + ".zip");
                allPackage.DownloadName = AssetPackagingHelper.MakeValidFileName (asset.Name + ".zip");
                packages.Add (allPackage);

                _logger.LogDebug ($"Created package for ALL files. Hash: {allPackage.Hash}");
                //create package filegroups (files that have the same name, but different extensions)
                var fileGroups = allPackage.Files.GroupBy (x => x.Name);
                _logger.LogDebug ($"{fileGroups.Count()} file groups found. Creating package for each group.");
                foreach (var group in fileGroups) {
                    if (group.Count () < 2) {
                        //no need to package if there's only one file with the same name
                        _logger.LogDebug ($"Group {group.Key} only has one package, no need to package.");
                        continue;
                    }

                    //create temp group directory
                    var tempGrpDirectory = Path.Combine (tempFilePath, group.Key);
                    Directory.CreateDirectory (tempGrpDirectory);
                    _logger.LogDebug ($"Created temp group directory at {tempGrpDirectory}");
                    //copy files with same name to temp group directory
                    foreach (var file in group) {
                        var copyTo = Path.Combine (tempGrpDirectory, Path.GetFileName (file.Path));
                        _logger.LogDebug ($"Copying {file.Path} to {copyTo}");
                        System.IO.File.Copy (file.Path, copyTo);
                    }

                    var grpPackage = new PackageModel ();
                    grpPackage.Hash = AssetPackagingHelper.ComputePackageHash (group.Select (x => x.Id).ToList ());
                    grpPackage.SourceDirectory = tempGrpDirectory;
                    grpPackage.DestinationDirectory = Path.Combine (tempPackagePath, grpPackage.Hash + ".zip");
                    grpPackage.DownloadName = AssetPackagingHelper.MakeValidFileName ($"{asset.Name}_{group.Key}.zip");
                    grpPackage.Files = group.ToList ();

                    packages.Add (grpPackage);
                    _logger.LogDebug ($"Created package for file with name {group.Key}. Hash: {grpPackage.Hash}");
                }

                //compress everything
                _logger.LogInformation ($"Compressing {packages.Count} package(s)");
                var packagePaths = new List<string> ();
                counter = 1;
                foreach (var package in packages) {
                    _logger.LogInformation ($"Package {counter} / {packages.Count} - Compressing...");
                    _logger.LogDebug ($"Package Hash: {package.Hash}");
                    _logger.LogDebug ($"Compressing {package.SourceDirectory}");
                    AssetPackagingHelper.CreatePackage (package);
                    _logger.LogDebug ($"Package compressed to {package.DestinationDirectory}");
                    counter++;
                }

                //upload package to s3
                _logger.LogInformation ($"Uploading {packages.Count} packages to S3");
                counter = 1;
                foreach (var package in packages) {
                    _logger.LogInformation ($"Package {counter} / {packages.Count} - Uploading...");
                    var key = _config.AWSPackagePath + asset.Id.ToLower () + "/" + Path.GetFileName (package.DestinationDirectory);
                    await _s3.UploadObjectAsync (package.DestinationDirectory, key, package.DownloadName);
                    _logger.LogInformation ($"Package {counter} / {packages.Count} - Successfully uploaded to {key}");
                }

                job.S3Location = _config.AWSPackagePath + asset.Id.ToLower ();

                _dbService.SetPackageHashOnAsset (job.AssetId, job.Hash);
                _dbService.MarkPackageJobAsComplete (job);
                disposeJob (jobId);
            } catch (Exception ex) {
                _logger.LogError ($"An error occured: {ex.Message}");
                _logger.LogError (ex.StackTrace);
                disposeJob (jobId);
                throw ex;
            }
        }

        public async Task DeleteExistingPackages (string assetId) {
            var s3Objects = await _s3.ListObjectsAsync (_config.AWSPackagePath + assetId);
            if (s3Objects == null || s3Objects.Count < 1) {
                return;
            }
            _logger.LogDebug ($"Deleting {s3Objects.Count} existing packages from S3 for Asset: {assetId}");
            await _s3.DeleteObjectsAsync (s3Objects.Select (x => x.Key).ToList ());
        }
    }
}