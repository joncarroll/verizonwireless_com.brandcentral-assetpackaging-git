using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using AssetPackaging.Domain;
using AssetPackaging.Domain.Models;
using AssetPackaging.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace AssetPackaging.Services {
    public class RLDbService : IRlDbService {

        private readonly ResourceLibraryContext _dbContext;
        private readonly ILogger<RLDbService> _logger;

        public System.Data.Common.DbConnection Connection { get; set; }
        public RLDbService (ResourceLibraryContext db, ILogger<RLDbService> logger) {
            this.Connection = db.Database.GetDbConnection ();
            _logger = logger;
            _dbContext = db;
            _logger.LogDebug ("Initializing RLDb Service.");
            _logger.LogDebug ("Server: " + Connection.DataSource);
            _logger.LogDebug ("Database: " + Connection.Database);
        }

        public List<FileVersion> GetCurrentFileVersions (Asset asset) {
            return asset.AssetFiles.Select (x => x.File)
                .Where (x => x.CurrentVersionId != null && (x.FileVersions != null && x.FileVersions.Count > 0))
                .Select (x => x.FileVersions.FirstOrDefault (y => y.Id == x.CurrentVersionId))
                .OrderBy (x => x.Id)
                .ToList ();
        }

        public Asset GetAsset (string id) {
            try {
                _logger.LogDebug ("GetAsset(string) Start");
                var retVal = _dbContext.Assets
                    .Include (x => x.AssetFiles)
                    .ThenInclude (x => x.File)
                    .ThenInclude (x => x.FileVersions)
                    .FirstOrDefault (x => x.Id.ToLower () == id.ToLower ());
                _logger.LogDebug ("GetAsset(string) End");
                return retVal;
            } catch (Exception ex) {
                _logger.LogInformation ($"An error occured: {ex.Message}");
                _logger.LogInformation ($"Stack Trace: {ex.StackTrace}");
                throw ex;
            }
        }
        public Asset GetAsset (Guid id) {
            try {
                _logger.LogDebug ("GetAsset(guid) Start");
                var retVal = _dbContext.Assets
                    .Include (x => x.AssetFiles)
                    .ThenInclude (x => x.File)
                    .ThenInclude (x => x.FileVersions)
                    .FirstOrDefault (x => x.Uid.Equals (id));
                _logger.LogDebug ("GetAsset(guid) End");
                return retVal;
            } catch (Exception ex) {
                _logger.LogInformation ($"An error occured: {ex.Message}");
                _logger.LogInformation ($"Stack Trace: {ex.StackTrace}");
                throw ex;
            }
        }
        public List<Asset> GetActiveAssets () {
            try {
                _logger.LogDebug ("GetActiveAssets Start");
                var retVal = _dbContext.Assets
                    .Include (x => x.AssetFiles)
                    .ThenInclude (x => x.File)
                    .ThenInclude (x => x.FileVersions)
                    .Where (x => x.Deactivated == 0 && x.ExpiresOn > DateTime.Now)
                    .ToList ();
                _logger.LogDebug ("GetActiveAssets End");
                return retVal;
            } catch (Exception ex) {
                _logger.LogInformation ($"An error occured: {ex.Message}");
                _logger.LogInformation ($"Stack Trace: {ex.StackTrace}");
                throw ex;
            }
        }

        public List<Asset> GetActiveStaleAssets () {
            try {
                _logger.LogDebug ("GetActiveStaleAssets Start");
                var retVal = _dbContext.Assets
                    .Include (x => x.AssetFiles)
                    .ThenInclude (x => x.File)
                    .ThenInclude (x => x.FileVersions)
                    .Where (x => x.Deactivated == 0 &&
                        x.ExpiresOn > DateTime.Now &&
                        string.IsNullOrEmpty (x.PackageHash) &&
                        (x.AssetFiles
                                .Select (x => x.File)
                                .Where(x => x.FileVersions.Any() && !string.IsNullOrEmpty(x.CurrentVersionId))
                        ).Count () > 1
                    )
                    .ToList ();
                _logger.LogDebug ("GetActiveStaleAssets End");
                return retVal;
            } catch (Exception ex) {
                _logger.LogInformation ($"An error occured: {ex.Message}");
                _logger.LogInformation ($"Stack Trace: {ex.StackTrace}");
                throw ex;
            }
        }

        public PackageJob CreatePackageJob (Guid assetId, string hash) {
            try {
                _logger.LogDebug ("CreatePackageJob Start");
                var pj = new PackageJob () {
                    AssetId = assetId,
                    DateCreated = DateTime.Now,
                    HasError = false,
                    RetryCount = 0,
                    Completed = false,
                    Started = false,
                    Hash = hash,
                    AssetIdString = assetId.ToString ()
                };
                _dbContext.PackageJobs.Add (pj);
                _dbContext.SaveChanges ();
                _logger.LogDebug ("CreatePackageJob End");
                return pj;
            } catch (Exception ex) {
                _logger.LogInformation ($"An error occured: {ex.Message}");
                _logger.LogInformation ($"Stack Trace: {ex.StackTrace}");
                throw ex;
            }
        }

        public void DeletePackageJob (int id) {
            try {
                _logger.LogDebug ("DeletePackageJob Start");
                var pj = _dbContext.PackageJobs.FirstOrDefault (x => x.Id == id);
                _dbContext.PackageJobs.Remove (pj);
                _dbContext.SaveChanges ();
                _logger.LogDebug ("DeletePackageJob End");
            } catch (Exception ex) {
                _logger.LogInformation ($"An error occured: {ex.Message}");
                _logger.LogInformation ($"Stack Trace: {ex.StackTrace}");
                throw ex;
            }
        }

        public void DeletePackageJob (PackageJob pj) {
            try {
                _logger.LogDebug ("DeletePackageJob Start");
                _dbContext.PackageJobs.Remove (pj);
                _dbContext.SaveChanges ();
                _logger.LogDebug ("DeletePackageJob End");
            } catch (Exception ex) {
                _logger.LogInformation ($"An error occured: {ex.Message}");
                _logger.LogInformation ($"Stack Trace: {ex.StackTrace}");
                throw ex;
            }
        }

        public void MarkPackageJobAsStarted (PackageJob pj) {
            try {
                _logger.LogDebug ("MarkPackageJobAsStarted Start");
                pj.Started = true;
                pj.DateStarted = DateTime.Now;
                _dbContext.PackageJobs.Update (pj);
                _dbContext.SaveChanges ();
                _logger.LogDebug ("MarkPackageJobAsStarted End");
            } catch (Exception ex) {
                _logger.LogInformation ($"An error occured: {ex.Message}");
                _logger.LogInformation ($"Stack Trace: {ex.StackTrace}");
                throw ex;
            }
        }
        public void MarkPackageJobAsComplete (PackageJob pj) {
            try {
                _logger.LogDebug ("MarkPackageJobAsComplete Start");
                pj.Completed = true;
                pj.DateCompleted = DateTime.Now;
                pj.HasError = false;
                pj.ErrorMessage = string.Empty;
                pj.StackTrace = string.Empty;
                _dbContext.PackageJobs.Update (pj);
                _dbContext.SaveChanges ();
                _logger.LogDebug ("MarkPackageJobAsComplete End");
            } catch (Exception ex) {
                _logger.LogInformation ($"An error occured: {ex.Message}");
                _logger.LogInformation ($"Stack Trace: {ex.StackTrace}");
                throw ex;
            }
        }
        public void MarkPackageJobAsErrored (PackageJob pj, Exception error) {
            try {
                _logger.LogDebug ("MarkPackageJobAsErrored Start");
                var errorMsg = new StringBuilder ();
                var stackTrace = new StringBuilder ();
                errorMsg.AppendLine ($"Message: {error.Message}");
                stackTrace.AppendLine (error.StackTrace);
                if (error.InnerException != null) {
                    errorMsg.AppendLine ($"Inner Exception: {error.InnerException.Message}");
                    stackTrace.AppendLine ($"Inner Stack Trace: {error.InnerException.StackTrace}");
                }
                pj.ErrorMessage = errorMsg.ToString ();
                pj.StackTrace = stackTrace.ToString ();

                pj.HasError = true;

                _dbContext.PackageJobs.Update (pj);
                _dbContext.SaveChanges ();
                _logger.LogDebug ("MarkPackageJobAsErrored End");
            } catch (Exception ex) {
                _logger.LogInformation ($"An error occured: {ex.Message}");
                _logger.LogInformation ($"Stack Trace: {ex.StackTrace}");
                throw ex;
            }
        }

        public List<PackageJob> GetErroredPackageJobs (int maxRetryCount) {
            try {
                _logger.LogDebug ("GetErroredPackageJobs Start");
                var retVal = _dbContext.PackageJobs.Where (x => x.HasError && x.RetryCount < maxRetryCount).ToList ();
                _logger.LogDebug ("GetErroredPackageJobs End");
                return retVal;
            } catch (Exception ex) {
                _logger.LogInformation ($"An error occured: {ex.Message}");
                _logger.LogInformation ($"Stack Trace: {ex.StackTrace}");
                throw ex;
            }
        }

        public PackageJob IncrementRetryCount (PackageJob pj) {
            try {
                _logger.LogDebug ("IncrementRetryCount Start");
                pj.RetryCount = pj.RetryCount + 1;

                _dbContext.PackageJobs.Update (pj);
                _dbContext.SaveChanges ();
                _logger.LogDebug ("IncrementRetryCount End");
                return pj;

            } catch (Exception ex) {
                _logger.LogInformation ($"An error occured: {ex.Message}");
                _logger.LogInformation ($"Stack Trace: {ex.StackTrace}");
                throw ex;
            }
        }

        public PackageJob GetPackageJob (Guid assetId, string hash) {
            try {
                _logger.LogDebug ("GetPackageJob Start");
                var retVal = _dbContext.PackageJobs.FirstOrDefault (x => x.AssetId == assetId && x.Hash == hash);
                _logger.LogDebug ("GetPackageJob End");
                return retVal;
            } catch (Exception ex) {
                _logger.LogInformation ($"An error occured: {ex.Message}");
                _logger.LogInformation ($"Stack Trace: {ex.StackTrace}");
                throw ex;
            }
        }

        public List<PackageJob> GetPendingJobs () {
            try {
                _logger.LogDebug ("GetPendingJobs Start");
                var retVal = _dbContext.PackageJobs.Where (x => x.Completed == false && x.Started == false && x.HasError == false).ToList ();
                _logger.LogDebug ("GetPendingJobs End");
                return retVal;
            } catch (Exception ex) {
                _logger.LogInformation ($"An error occured: {ex.Message}");
                _logger.LogInformation ($"Stack Trace: {ex.StackTrace}");
                throw ex;
            }
        }

        public void SetPackageHashOnAsset (Guid assetId, string hash) {
            try {
                _logger.LogDebug ("SetPackageHashOnAsset Start");
                var asset = _dbContext.Assets.FirstOrDefault (x => x.Uid == assetId);
                asset.PackageHash = hash;
                _dbContext.Assets.Update (asset);
                _dbContext.SaveChanges ();
                _logger.LogDebug ("SetPackageHashOnAsset End");
            } catch (Exception ex) {
                _logger.LogInformation ($"An error occured: {ex.Message}");
                _logger.LogInformation ($"Stack Trace: {ex.StackTrace}");
                throw ex;
            }
        }
        public void SetPackageHashOnAsset (Asset asset, string hash) {
            try {
                _logger.LogDebug ("SetPackageHashOnAsset Start");
                asset.PackageHash = hash;
                _dbContext.Assets.Update (asset);
                _dbContext.SaveChanges ();
                _logger.LogDebug ("SetPackageHashOnAsset End");
            } catch (Exception ex) {
                _logger.LogInformation ($"An error occured: {ex.Message}");
                _logger.LogInformation ($"Stack Trace: {ex.StackTrace}");
                throw ex;
            }
        }
    }
}