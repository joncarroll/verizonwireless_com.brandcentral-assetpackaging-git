using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AssetPackaging.Domain.Models;

namespace AssetPackaging.Services.Interfaces {
    public interface IPackagingService {
        long GetSizeOfAllActiveFiles ();
        Task<int> RefreshJobs ();
        Task RunPendingJobs ();
        Task RunJob (PackageJob job);
        Task RetryJobs ();
        Task DeleteExistingPackages (string assetId);
    }
}