using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AssetPackaging.Domain.Models;

namespace AssetPackaging.Services.Interfaces {
    public interface IRlDbService {
        List<Asset> GetActiveAssets ();
        List<Asset> GetActiveStaleAssets ();
        Asset GetAsset (string id);
        Asset GetAsset (Guid id);
        List<FileVersion> GetCurrentFileVersions (Asset asset);
        PackageJob CreatePackageJob (Guid assetId, string hash);
        void DeletePackageJob (int id);
        void DeletePackageJob (PackageJob pj);
        void MarkPackageJobAsComplete (PackageJob pj);
        void MarkPackageJobAsStarted (PackageJob pj);
        void MarkPackageJobAsErrored (PackageJob pj, Exception error);
        List<PackageJob> GetErroredPackageJobs (int maxRetryCount);
        PackageJob IncrementRetryCount (PackageJob pj);
        PackageJob GetPackageJob (Guid assetId, string hash);
        List<PackageJob> GetPendingJobs ();
        void SetPackageHashOnAsset (Guid assetId, string hash);
        void SetPackageHashOnAsset (Asset asset, string hash);
    }
}