using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Amazon.S3.Model;

namespace AssetPackaging.Services.Interfaces {
    public interface IS3Service {
        Task<MemoryStream> GetObjectMemoryStreamAsync (string key);
        Task DownloadObjectAsync (string key, string path);
        Task<GetObjectMetadataResponse> GetObjectMetadataAsync (string key);
        Task<List<GetObjectMetadataResponse>> GetObjectMetadataAsync (List<string> keys);
        Task UploadObjectAsync (string filePath, string destinationKey, string downloadName);
        Task<List<S3Object>> ListObjectsAsync (string prefix);
        Task DeleteObjectsAsync (List<string> objectKeys);
    }
}