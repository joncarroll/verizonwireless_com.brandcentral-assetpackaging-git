
/****** Object:  Table [dbo].[tbl_PackageJob]    Script Date: 10/22/2019 2:35:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tbl_PackageJob](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AssetId] [uniqueidentifier] NOT NULL,
	[Completed] [bit] NOT NULL,
	[Started] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateCompleted] [datetime] NULL,
	[HasError] [bit] NOT NULL,
	[ErrorMessage] [varchar](max) NULL,
	[StackTrace] [varchar](max) NULL,
	[RetryCount] [int] NOT NULL,
	[Hash] [varchar](50) NOT NULL,
	[DateStarted] [datetime] NULL,
	[AssetIdString] [varchar](50) NULL,
	[S3Location] [varchar](200) NULL,
 CONSTRAINT [PK_tbl_PackageJobs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[tbl_PackageJob] ADD  CONSTRAINT [DF_tbl_PackageJob_Started]  DEFAULT ((0)) FOR [Started]
GO

ALTER TABLE [dbo].[tbl_PackageJob] ADD  CONSTRAINT [DF_tbl_PackageJob_HasError]  DEFAULT ((0)) FOR [HasError]
GO

/* Add Package Hash Column to Asset Table */
ALTER TABLE tbl_Asset
ADD PackageHash varchar(50);
GO

