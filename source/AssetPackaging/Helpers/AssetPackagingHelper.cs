using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using AssetPackaging.Models;

namespace AssetPackaging.Helpers {
    public static class AssetPackagingHelper {
        public static string ComputePackageHash (List<string> fileIds) {
            var item = string.Join ("|", fileIds.OrderBy(x => x));
            char[] padding = { '=' };
            using (var sha256 = SHA256.Create ()) {
                var hashedResult = sha256.ComputeHash (Encoding.UTF8.GetBytes (item));
                var base64Result = Convert.ToBase64String (hashedResult);
                //replace / and + to make safe for filenames
                return base64Result.TrimEnd (padding).Replace ("+", "-").Replace ("/", "_");
            }
        }

        public static void CreatePackage (PackageModel package) {
            ZipFile.CreateFromDirectory (
                package.SourceDirectory,
                package.DestinationDirectory
            );
        }
        public static string MakeValidFileName (string name) {
            string invalidChars = System.Text.RegularExpressions.Regex.Escape (new string (System.IO.Path.GetInvalidFileNameChars ()));
            string invalidRegStr = string.Format (@"([{0}]*\.+$)|([{0}]+)", invalidChars);

            return System.Text.RegularExpressions.Regex.Replace (name, invalidRegStr, "_");
        }
    }
}