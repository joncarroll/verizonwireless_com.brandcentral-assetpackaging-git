namespace AssetPackaging.Config
{

    public class AWSConfig {
        public string Region { get; set; }
        public string SecretKey { get; set; }
        public string AccessKey { get; set; }
        public string Bucket { get; set; }
    }
}