namespace AssetPackaging.Config {

    public class AppConfig {
        public string AWSFilesPath { get; set; }
        public string AWSPackagePath { get; set; }
        public string LocalFilesPath { get; set; }
        public string LocalPackagePath { get; set; }
        public string TestAssetId { get; set; }
        public bool DisposeLocalFiles { get; set; }
        public int MaxRetryCount { get; set; }
    }
}