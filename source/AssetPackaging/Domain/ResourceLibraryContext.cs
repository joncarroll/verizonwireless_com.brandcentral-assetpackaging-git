using AssetPackaging.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Logging;

namespace AssetPackaging.Domain {
    public class ResourceLibraryContext : DbContext {
        public ResourceLibraryContext (DbContextOptions<ResourceLibraryContext> options) : base (options) {

        }

        protected override void OnModelCreating (ModelBuilder modelBuilder) {
            modelBuilder.Entity<AssetFile> ()
                .HasKey (af => new { af.AssetId, af.FileId });

            modelBuilder.Entity<AssetFile> ()
                .HasOne<Asset> (a => a.Asset)
                .WithMany (x => x.AssetFiles)
                .HasForeignKey (x => x.AssetId);

            modelBuilder.Entity<AssetFile> ()
                .HasOne<File> (f => f.File)
                .WithMany ()
                .HasForeignKey (x => x.FileId);

            modelBuilder.Entity<FileVersion> ()
                .HasOne (x => x.File)
                .WithMany (x => x.FileVersions)
                .HasForeignKey (x => x.FileId);
        }

        protected override void OnConfiguring (DbContextOptionsBuilder optionsBuilder) => optionsBuilder.ConfigureWarnings (c => c.Log ((RelationalEventId.CommandExecuting, LogLevel.Trace)));

        public DbSet<Asset> Assets { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<FileVersion> FileVersions { get; set; }
        public DbSet<AssetFile> AssetFiles { get; set; }
        public DbSet<PackageJob> PackageJobs { get; set; }
    }
}