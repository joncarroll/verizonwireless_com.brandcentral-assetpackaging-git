using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssetPackaging.Domain.Models {
    [Table ("tbl_Asset")]
    public class Asset {

        [Column("Asset_ID")]
        public string Id { get; set; }
        [Column("Asset_ID_UI")]
        public Guid Uid { get; set; }

        [Column("Asset_Name")]
        public string Name { get; set; }

        [Column("Asset_Deactivate")]
        public int Deactivated { get; set; }

        [Column("Asset_Expiration")]
        public DateTime ExpiresOn { get; set; }

        [Column("PackageHash")]
        public string PackageHash { get; set; }

        public ICollection<AssetFile> AssetFiles { get; set; }

    }
}