using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssetPackaging.Domain.Models {
    [Table ("tbl_File_Version")]
    public class FileVersion {

        [Column("Version_ID")]
        public string Id { get; set; }

        [Column("File_ID")]
        public string FileId { get; set; }

        [Column("Version_FileName")]
        public string Name { get; set; }

        [Column("Version_Extension")]
        public string Extension { get; set; }

        [Column("Version_CreateDate")]
        public DateTime CreateDate { get; set; }

        public File File { get; set; }

        public string GenerateAwsKey(string prefix) {
            return $"{prefix}RL{this.Id}{this.Extension}";
        }
    }
}