using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssetPackaging.Domain.Models {
    [Table ("tbl_Asset_File")]
    public class AssetFile {

        [Column ("Asset_ID")]
        public string AssetId { get; set; }
        public Asset Asset { get; set; }


        [Column ("File_ID")]
        public string FileId { get; set; }
        public File File { get; set; }

    }
}