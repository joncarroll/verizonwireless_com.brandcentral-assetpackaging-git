using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssetPackaging.Domain.Models {
    [Table ("tbl_PackageJob")]
    public class PackageJob {

        public bool IsPending {
            get {
                return (Completed == false && HasError == false && Started == false);
            }
        }

        [Column ("Id")]
        [Key]
        [DatabaseGenerated (DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column ("AssetId")]
        public Guid AssetId { get; set; }

        [Column ("Hash")]
        public string Hash { get; set; }

        [Column ("Completed")]
        public bool Completed { get; set; }

        [Column ("Started")]
        public bool Started { get; set; }

        [Column ("DateCreated")]
        public DateTime DateCreated { get; set; }

        [Column ("DateCompleted")]
        public DateTime? DateCompleted { get; set; }

        [Column ("DateStarted")]
        public DateTime? DateStarted { get; set; }

        [Column ("HasError")]
        public bool HasError { get; set; }

        [Column ("ErrorMessage")]
        public string ErrorMessage { get; set; }

        [Column ("StackTrace")]

        public string StackTrace { get; set; }

        [Column ("RetryCount")]

        public int RetryCount { get; set; }

        [Column ("AssetIdString")]
        public string AssetIdString { get; set; }

        [Column ("S3Location")]
        public string S3Location { get; set; }
    }
}