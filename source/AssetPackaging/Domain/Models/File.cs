using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace AssetPackaging.Domain.Models {
    [Table ("tbl_File")]
    public class File {

        [Column ("File_ID")]
        public string Id { get; set; }

        [Column ("File_FileName")]
        public string Name { get; set; }

        [Column("File_Extension")]
        public string Extension { get; set; }
        
        [Column("Current_File_Version_ID")]
        public string CurrentVersionId { get; set; }

        public ICollection<FileVersion> FileVersions { get; set; }

    }
}