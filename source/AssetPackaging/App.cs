using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Amazon.S3.Model;
using AssetPackaging.Config;
using AssetPackaging.Domain;
using AssetPackaging.Services;
using AssetPackaging.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace AssetPackaging {
    public class App {
        private readonly ILogger<App> _logger;
        private readonly IPackagingService _packagingService;
        private readonly AppConfig _config;

        public App (ILogger<App> logger, IPackagingService packagingService, IOptions<AppConfig> config) {
            _logger = logger;
            _packagingService = packagingService;
            _config = config.Value;
        }

        public async Task Run (string[] args) {
            _logger.LogInformation ($"App Starting...");

            if (await _packagingService.RefreshJobs () > 0) {
                await _packagingService.RunPendingJobs ();
            }

            _logger.LogInformation ("App Shutting Down...");
        }

    }
}